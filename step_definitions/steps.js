const { I } = inject();
const assert = require('assert');
let data = null
// Add in your custom step files

Given('I ', () => {
  // TODO: replace with your own step
});


Given('I launch the checkout application', () => {
  I.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')
});

Then('Read the data from the data file- {string}', async (fileName) => {
  data = require(`../data/${fileName}`);
  console.log(data);
});

Then('I verify checkout page title is {string}', async (expectedTitle) => {
  const currentTitle = await I.grabTitle();
  console.log(`Current page title is "${currentTitle}"`);
  console.log(`Expected page title is "${expectedTitle}"`);
  assert.strictEqual(currentTitle, expectedTitle, `Expected page title "${expectedTitle}", but found "${currentTitle}"`);
});

Then('I fill checkout page form', async () => {
  await I.fillField(`#firstName`, data.firstName)
  await I.fillField(`#lastName`, data.lastName)
  await I.fillField(`#username`, data.username)
  pause();
});


