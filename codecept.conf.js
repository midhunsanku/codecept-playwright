exports.config = {
  output: './output',
  helpers: {
    Playwright: {
      browser: 'chromium',
      url: 'http://localhost',
      show: true,
      video: true,
      keepVideoForPassedTests: false,
      fullPageScreenshots: true,
      disableScreenshots: true,
      uniqueScreenshotNames: true,
      waitForAction: 500,
      timeout: 1000,
      windowSize: '1200x900'
    },
    Common: {
      require: './helpers/common_helper.js'
    },
    mochawesome: {
      uniqueScreenshotNames: true
    }
  },
  include: {
    I: './steps_file.js'
  },
  mocha: {
    reporter: 'mochawesome',
    reporterOptions: {
      reportDir: './output',
      reportTitle: 'Test Report',
      uniqueFilename: true
    }
  },
  bootstrap: null,
  timeout: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: ['./step_definitions/steps.js']
  },
  plugins: {
    screenshotOnFail: {
      enabled: false
    },
    debugErrors: {
      enabled: true
    },
    pauseOnFail: {},
    tryTo: {
      enabled: true
    },
    stepByStepReport: {
      enabled: true,
      screenshotsForAllureReport: true,
      deleteSuccessful: false
    },
    subtitles: {
      enabled: true
    },
    retryTo: {
      enabled: true
    },
    retryFailedStep: {
      enabled: true,
      retries: 3,
      minTimeout: 1000,
      maxTimeout: 5000
    },
    eachElement: {
      enabled: true
    }
  },
  stepTimeout: 0,
  stepTimeoutOverride: [{
      pattern: 'wait.*',
      timeout: 0
    },
    {
      pattern: 'amOnPage',
      timeout: 0
    }
  ],
  tests: './features/*_test.js',
  grep: null,
  retry: 5,
  name: 'codecept-playwright'
}