const Helper = require('@codeceptjs/helper');

class Common extends Helper {

  // before/after hooks
  /**
   * @protected
   */
  _before() {
    // remove if not used
    this.playwright = this.helpers['Playwright']

  }

  /**
   * @protected
   */
  _after() {
    // remove if not used
  }

  // add custom methods here
  // If you need to access other helpers
  // use: this.helpers['helperName']

  async launch() {
    await this.playwright.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')
    await this.playwright.see('Checkout form');
    console.log('Checkout form is visible on the page')
  }

  async fillField(locator, value) {
    await this.playwright.waitForElement(locator, 5)
    await this.playwright.fillField(locator, value)
    await this.playwright.pressKey('Tab')
  }

  async fillOutCheckoutForm() {
    await this.fillField(`#firstName`, `John`)

    await this.playwright.wait(5)

    // Using css
    await this.fillField("input[id='lastName']", `Doe`)
    await this.playwright.seeInField(`#firstName`, `John`)
    // Using Xpath
    await this.fillField("//input[@id='username']", `JohnDoe`)
    // using Axes
    await this.fillField(`//div[@class="input-group has-validation"]/following::input[@type='email']`, 'JohnDoe@gmail.com')
    // Using Conditional 
    await this.fillField('//input[@id="address" and @type="text" ]', '123 Main Street')

    await this.playwright.selectOption(`#country`, 'United States')
    await this.playwright.selectOption(`#state`, 'California')
    // using Wildcard
    await this.fillField('//*[@id="zip"]', '12345')
    await this.playwright.checkOption(`#same-address`)
    await this.playwright.click('//input[@id="debit"]')
  }

}

module.exports = Common;
