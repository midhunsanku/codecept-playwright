Feature: Checkout Flow
Launch the checkout application
Verify checkout page title
Fill checkout page form

Scenario: do something
  Given I launch the checkout application
  Then Read the data from the data file- "data.json"
  Then I verify checkout page title is "Checkout example · Bootstrap v5.1"
  Then I fill checkout page form
