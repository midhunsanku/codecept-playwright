const assert = require('assert');
const { stat } = require('fs');

Feature('CodeceptJS demo');

Scenario('CodeceptJS waits', async ({ I }) => {

  await I.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')
  await I.wait(1)
  await I.waitInUrl('https://getbootstrap.com/docs/5.1/examples/checkout/', 20)
  await I.fillField('#firstName', 'John')

  const status = await tryTo(async () => {
    I.fillField('#lastName', 'Doe')
  }, 5);

  if (status === 'failed') {
    console.log('Failed to fill the field')
  }

  await retryTo((tryNum) => {
    I.click('#lastName1', "John")
  }, 10);
  // await I.waitForElement('#firstName', 10)
  // await I.waitForClickable('#same-address', 5)
  // I.click('#same-address')
  // await I.waitForVisible('#lastName', 5) 
  // await I.waitForText('Checkout form', 5)
  /*
  I.waitForInvisible('#lastName', 5)
  I.waitForHidden('#lastName', 5)
  I.waitForDetached('#lastName', 5)
  I.waitForCookie('cookieName', 5)
  */
}).tag('@waits')