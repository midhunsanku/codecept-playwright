const assert = require('assert');

Feature('CodeceptJS demo');

/*Scenario('Actions and Interactive pause', ({ I }) => {
    I.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')

    I.see('Checkout form');
    // Using ID
    I.fillField(`#firstName`, `John`)
    
    // Using css
    I.fillField("input[id='lastName']", `Doe`)
    I.seeInField(`#firstName`, `John`)
    // Using Xpath
    I.fillField("//input[@id='username']", `JohnDoe`)
    // using Axes
    I.fillField(`//div[@class="input-group has-validation"]/following::input[@type='email']`, 'JohnDoe@gmail.com')
    // Using Conditional 
    I.fillField('//input[@id="address" and @type="text" ]', '123 Main Street')

    I.selectOption(`#country`, 'United States')
    I.selectOption(`#state`, 'California')
    // using Wildcard
    I.fillField('//*[@id="zip"]', '12345')
    I.checkOption(`#same-address`)
    I.click('//input[@id="debit"]')
    console.log('Checkout form is visible on the page')
}).tag('second');*/

Scenario('Mocha Awesome Report', ({ I }) => {
    I.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')
    I.see('Checkout form');
    // Using ID
    I.fillField(`#firstName`, `John`)
    // Using css
    I.fillField("input[id='lastName']", `Doe`)
    // Using Xpath
    I.fillField("//input[@id='username']", `JohnDoe`)
    I.saveScreenshot('checkout.png')
    I.wait(2)
    I.addMochawesomeContext('All scroll tests passed! 🎉🎉🎉');
    I.addMochawesomeContext({ title: 'Checkout form Screen shot', value: `./checkout.png` });
}).tag('mocha');