Feature('CodeceptJS demo');

Scenario('First Scenario', ({ I }) => {
    I.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')
    I.see('Checkout form Notebook');
    console.log('Checkout form is visible on the page')
}).tag('@full').tag('@mini');;


Scenario('Second scenario', ({ I }) => {
    I.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')
    I.see('Checkout form');
    console.log('Checkout form is visible on the page')
}).tag('@reg').tag('@smoke');;

Scenario('Third Scenario', ({ I }) => {
    I.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')
    I.see('Checkout form note book');
    console.log('Checkout form is visible on the page')
}).tag('@full').tag('@mini');;
