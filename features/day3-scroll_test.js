const assert = require('assert');

Feature('CodeceptJS demo');

Scenario('Scroll from checkout', async ({ I }) => {

    await I.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')
    const title = await I.grabTitle();
    assert.equal(title, 'Checkout example · Bootstrap v5.1');
    await I.scrollPageToBottom();
    await I.saveScreenshot("screenshot1.png");
    await I.wait(2);
    await I.scrollPageToTop();
    await I.saveScreenshot("screenshot2.png");
    await I.wait(2);
    await I.scrollTo('#credit');
    await I.saveScreenshot("screenshot3.png");
    await I.wait(5);
    await I.addMochawesomeContext('All scroll tests passed! 🎉🎉🎉');
    await I.addMochawesomeContext({ title: 'Screenshot 1', value: `./screenshot1.png` });
    await I.addMochawesomeContext({ title: 'Screenshot 2', value: `./screenshot2.png` });
    await I.addMochawesomeContext({ title: 'Screenshot 3', value: `./screenshot3.png` });
    await I.wait(2);
}).tag('@scroll')