Feature('CodeceptJS demo');

Scenario('Key actions from checkout', async ({ I }) => {
    await I.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')
    await I.click('#firstName')
    await I.pressKey('a')
    await I.wait(2)
    await I.fillField('#firstName', 'John')
    await I.wait(2)
    await I.pressKey(['Control', 'a'])
    await I.pressKey(['Control', 'c'])
    await I.wait(2)
    await I.pressKey('Tab')
    await I.pressKey(['Control', 'v'])
    await I.wait(5)

    await I.pressKeyDown('Backspace')
    await I.pressKeyUp('Backspace')
    await I.wait(5)
    await I.refreshPage()
    pause()
}).tag('@actions')


/*
Supported key actions are:

'AltLeft' or 'Alt'
'AltRight'
'ArrowDown'
'ArrowLeft'
'ArrowRight'
'ArrowUp'
'Backspace'
'Clear'
'ControlLeft' or 'Control'
'ControlRight'
'Command'
'CommandOrControl'
'Delete'
'End'
'Enter'
'Escape'
'F1' to 'F12'
'Home'
'Insert'
'MetaLeft' or 'Meta'
'MetaRight'
'Numpad0' to 'Numpad9'
'NumpadAdd'
'NumpadDecimal'
'NumpadDivide'
'NumpadMultiply'
'NumpadSubtract'
'PageDown'
'PageUp'
'Pause'
'Return'
'ShiftLeft' or 'Shift'
'ShiftRight'
'Space'
'Tab' */