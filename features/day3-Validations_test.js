Feature('CodeceptJS demo');

Scenario('Verify-Validation ', ({ I }) => {
    I.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')

    I.fillField(`#firstName`, `John`)
    I.fillField("input[id='lastName']", `Doe`)
    I.fillField("//input[@id='username']", `JohnDoe`)
    I.fillField(`//div[@class="input-group has-validation"]/following::input[@type='email']`, 'JohnDoe@gmail.com')
    I.fillField('//input[@id="address" and @type="text" ]', '123 Main Street')
    I.selectOption(`#country`, 'United States')
    I.selectOption(`#state`, 'California')
    I.fillField('//*[@id="zip"]', '12345')
    I.click(`//input[@id="same-address"]`)
    I.click('//input[@id="debit"]')

    I.wait(5)


    // Validations
    I.seeInTitle('Checkout example · Bootstrap')
    I.seeTitleEquals('Checkout example · Bootstrap v5.1')
    I.see('Your cart');
    I.seeInField(`#firstName`, `John`)
    I.seeCheckboxIsChecked(`//input[@id="same-address"]`)
    I.seeInCurrentUrl('checkout')
    I.seeElement(`//input[@id="credit"]`)
    I.seeCurrentUrlEquals('https://getbootstrap.com/docs/5.1/examples/checkout/')

}).tag('validation');