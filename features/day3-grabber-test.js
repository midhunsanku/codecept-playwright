const { output } = require("codeceptjs");
const assert = require('assert');

Feature('CodeceptJS demo');

Scenario('Grab from checkout', async ({ I }) => {
    await I.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')

    const title = await I.grabTitle()

    assert.equal(title, 'Checkout example · Bootstrap v5.1', `Checkout example · Bootstrap v5.1' but found ${title}`);

    const text = await I.grabTextFrom("//label[@for='firstName']")
    assert.equal(text, 'First name', `Expected text to be 'First name' but found ${text}`);


    const textAll = await I.grabTextFromAll("//select[@id='country']/option")
    console.log("Country options: "+ textAll);

    const value = await I.grabValueFrom('input[id=email]')
    assert.equal(value, '', `Expected value to be '' but found ${value}`);


    // await I.grabValueFromAll('')


    const noOfElement = await I.grabNumberOfVisibleElements("//input")
    console.log("Number of input elements: " + noOfElement);


    const attribute = await I.grabAttributeFrom('//input', "placeholder");
    console.log("Place holder attribute value: " + attribute);

    const attributeALl = await I.grabAttributeFromAll('//select', "id");
    console.log("Select element id attribute values: " + attributeALl);

    const attribute1 = await I.grabAttributeFrom('//input[@id="credit"]', "checked");
    const attribute2 = await I.grabAttributeFrom('//input[@id="debit"]', "checked");
    if (attribute1 === null) {
        console.log("Credit card is not selected");
    } else {
        console.log("Credit card is selected");
    }

    if (attribute2 === null) {
        console.log("Debit card is not selected");
    } else {
        console.log("Debit card is selected");
    }


    const currentURL = await I.grabCurrentUrl()
    console.log("Current URL: " + currentURL);

    const NoOfOpenTabs = await I.grabNumberOfOpenTabs()
    console.log("Number of open tabs: " + NoOfOpenTabs);

    const cookie = await I.grabCookie('cookieName')
    console.log("Cookie value: " + cookie);
}).tag('@grab')