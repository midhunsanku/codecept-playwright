Feature('CodeceptJS demo');

Scenario('check Welcome page on site', ({ I }) => {
    I.amOnPage('https://getbootstrap.com/docs/5.1/examples/checkout/')
    I.see('Checkout form');
    I.wait(5);
    console.log('Checkout form is visible on the page')
}).tag('@reg').tag('@smoke');
