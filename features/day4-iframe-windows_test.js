const assert = require('assert');

Feature('Day 4 - Iframe and Windows Handling');

Scenario('Iframe Handling', async ({ I }) => {
    await I.amOnPage('https://nunzioweb.com/iframes-example.htm')
    const iframeCount = await I.executeScript(() => {
        return document.querySelectorAll('iframe').length;
    });
    console.log("Total number of iframes: " + iframeCount)
    await I.wait(2)
    await I.switchTo('iframe[src="001.htm"]')
    await I.wait(2)
    await I.click('[title="Play/Pause"]')
    await I.wait(2)
    await I.switchTo()
    await I.switchTo('iframe[src="001-opacity-example.htm"]')
    const text = await I.grabAttributeFrom('img[src="images/mainkill.jpg"]', 'alt')
    await I.wait(2)

}).tag('@iframe')


// Scenario('New window Handling', async ({ I }) => {
//     await I.amOnPage('https://www.capterra.com/sem-compare/quality-management-software/?utm_source=ps-google&utm_medium=ppc&utm_campaign=:1:CAP:2:COM:3:All:4:US:5:BAU:6:SOF:7:Desktop:8:BR:9:Quality_Management&network=g&gclid=EAIaIQobChMI4omt9IrFhgMV86TCCB1I-CAeEAAYAiAAEgK18PD_BwE')
//     await I.wait(2)
//     await I.click('//span[text()="MasterControl Quality "]')
//     const tabs = await I.grabNumberOfOpenTabs()
//     console.log("Total number of tabs: " + tabs)
//     await I.switchToNextTab()
//     console.log(await I.grabTitle())
//     assert.equal(await I.grabTitle(), 'Quality Management System')
//     await I.closeCurrentTab()
//     const tabs1 = await I.grabNumberOfOpenTabs()
//     console.log("Total number of tabs: " + tabs1)
//     await I.openNewTab()
//     await I.switchToPreviousTab()
//     console.log(await I.grabTitle())
// }).tag('@windows');